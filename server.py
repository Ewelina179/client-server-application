import json
import socket
import threading
import time

IP_SERVER = "127.0.0.1"
port = 2738
UTF = "utf-8"
VERSION_OF_SERVER = "0.1.0"

MESSAGES = {
    "UPTIME": "LIFETIME OF SERVER",
    "INFO": "SERVER VERSION NUMBER, SERVER CREATION DATE",
    "HELP": {
        "LIST OF AVAILABLE COMMANDS": {
            "UPTIME": "LIFETIME OF SERVER",
            "INFO": "SERVER VERSION NUMBER, SERVER CREATION DATE",
            "STOP": "SERVER AND CLIENT DISCONNECTION",
        }
    },
    "STOP": "SERVER DISCONNECTION",
}

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind((IP_SERVER, port))
begin = time.time()
begin2 = time.ctime(time.time())
print("Connection time: ", begin2)
print("Server IP address", IP_SERVER)


def start():
    server.listen()
    while True:
        conn, address = server.accept()
        print("Client from address: ", address)
        thread = threading.Thread(target=client, args=(conn, address))
        thread.start()


def client(conn, address):
    connected = True
    while connected:
        end = time.time()
        end2 = time.ctime(end)
        print("Time of connection with client: ", end2)
        data = conn.recv(1024)
        data = data.decode(UTF)
        data = json.loads(data)
        print(data)
        print(data["message"])
        a = data["message"]
        func = {
            "INFO": info(begin2),
            "UPTIME": uptime(end, begin),
            "HELP": help(),
            "STOP": stop(),
        }
        data_to = func.get(a)

        data_to_server = json.dumps(data_to)
        print(data_to)
        conn.send(bytes(data_to_server, encoding=UTF))
        if data["message"] == "STOP":
            server.close()


def uptime(end, begin):
    now = time.time()
    uptime = now - begin
    MESSAGES["UPTIME"] = uptime
    return MESSAGES.fromkeys(["UPTIME"], uptime)


def info(begin2):
    info = {}
    info["SERVER VERSION NUMBER"] = VERSION_OF_SERVER
    info["DATE OF SERVER CREATION"] = begin2
    MESSAGES["INFO"] = info
    return MESSAGES.fromkeys(["INFO"], info)


def help():
    return MESSAGES["HELP"][
        "LIST OF AVAILABLE COMMANDS"
    ]  # help wysyła bez "help:"". ale chyba może być


def stop():
    return {"STOP": "SERVER DISCONNECTION"}


if __name__ == "__main__":
    start()
