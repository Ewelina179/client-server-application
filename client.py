import json
import socket

UTF = "utf-8"

COMM = ["UPTIME", "INFO", "HELP", "STOP"]

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(("localhost", 2738))


def client_send(y):
    data = {}
    for x in range(len(COMM)):
        if COMM[x] == y:
            data["message"] = y
            data = json.dumps(data)
            client.sendall(bytes(data, encoding=UTF))
            data = client.recv(1024)
            data = data.decode(UTF)
            print(repr(data))


def start():
    while True:
        print(
            "You can choose command from list. Type {} to get information about commands.".format(
                COMM[2]
            )
        )
        z = input()
        client_send(z)
        if z == "STOP":
            client.close()
            break


if __name__ == "__main__":
    start()
